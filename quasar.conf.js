// Configuration for your app
require('dotenv').config()
let apiUrl = process.env.API_URL
let localUrl = process.env.DEV_API_URL
var path = require('path')
module.exports = function (ctx) {
  return {
    // app plugins (/src/plugins)
    plugins: [
      'apollo',
      'user',
      'provider',
      'graphql',
      'entityChecks',
      'helpers',
      'variables',
      'Datefns',
      'memes',
      'queryBuilder'
    ],
    css: [
      'app.styl'
    ],
    extras: [
      ctx.theme.mat ? 'roboto-font' : null,
      'material-icons',
      ctx.theme.ios ? 'ionicons' : null,
      // 'mdi',
      // 'fontawesome'
    ],
    supportIE: true,
    build: {
      env: ctx.dev
        ? { // so on dev we'll have
          API: JSON.stringify(localUrl)
        }
        : { // and on build (production):
          API: JSON.stringify(apiUrl)
        },
      scopeHoisting: true,
      vueRouterMode: 'hash',
      // vueCompiler: true,
      // gzip: true,
      // analyze: true,
      // extractCSS: false,
      extendWebpack (cfg) {
        cfg.resolve.extensions.push('.ts')
        cfg.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules|quasar)/
        })
        cfg.module.rules.splice(0, 0, {
          test: /\.(ts)$/,
          loader: 'ts-loader',
          exclude: /(node_modules|quasar)/,
          options: {
            appendTsSuffixTo: [/\.vue$/]
          }
        })
        cfg.resolve.alias = {
          ...cfg.resolve.alias,
          // Add your own alias like this
          '@': path.resolve(__dirname, './src/plugins')
        }
      }
    },
    devServer: {
      // https: true,
      // port: 8080,
      open: true // opens browser window automatically
    },
    // framework: 'all' --- includes everything; for dev only!
    framework: 'all',
    // animations: 'all' --- includes all animations
    animations: 'all',
    pwa: {
      // workboxPluginMode: 'InjectManifest',
      // workboxOptions: {},
      manifest: {
        // name: 'Quasar App',
        // short_name: 'Quasar-PWA',
        // description: 'Best PWA App in town!',
        display: 'standalone',
        orientation: 'portrait',
        background_color: '#ffffff',
        theme_color: '#027be3',
        icons: [
          {
            'src': 'statics/icons/icon-128x128.png',
            'sizes': '128x128',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-192x192.png',
            'sizes': '192x192',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-256x256.png',
            'sizes': '256x256',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-384x384.png',
            'sizes': '384x384',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-512x512.png',
            'sizes': '512x512',
            'type': 'image/png'
          }
        ]
      }
    },
    cordova: {
      // id: 'org.cordova.quasar.app'
    },
    electron: {
      // bundler: 'builder', // or 'packager'
      extendWebpack (cfg) {
        // do something with Electron process Webpack cfg
      },
      packager: {
        // https://github.com/electron-userland/electron-packager/blob/master/docs/api.md#options

        // OS X / Mac App Store
        // appBundleId: '',
        // appCategoryType: '',
        // osxSign: '',
        // protocol: 'myapp://path',

        // Window only
        // win32metadata: { ... }
      },
      builder: {
        // https://www.electron.build/configuration/configuration

        // appId: 'quasar-app'
      }
    }
  }
}
