
import DataClient from '../provider/DataClient/DataClient'
import Tasks from '../provider/DataModels/Tasks/Tasks'
import Goals from '../provider/DataModels/Goals/Goals'
import Checklists from '../provider/DataModels/Checklists'
import Steps from '../provider/DataModels/Goals/Steps'
import Points from '../provider/DataModels/Checklists/Points'

export const types = {
  'Task': Tasks,
  'Goal': Goals,
  'Step': Steps,
  'Checklist': Checklists,
  'Point': Points
}
export const getEntityQuery = function (entity, queryType) {
  return types[entity.__typename][queryType]
}
export const getClient = function () {
  return DataClient
}
export const updateEntity = function (entity) {
  return types[entity.__typename].update(entity)
}
export const removeEntity = function (entity) {
  return types[entity.__typename].remove(entity)
}
export const createEntity = function (entity) {
  return types[entity.__typename].add(entity)
}
export default getEntityQuery
