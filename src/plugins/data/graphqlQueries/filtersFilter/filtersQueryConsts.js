export const excludedGroupings = {
  Project: {
    projects_none: {
      state: 'Excluded'
    }
  },
  Context: {
    contexts_none: {
      state: 'Excluded'
    }
  },
  Mood: {
    moods_none: {
      state: 'Excluded'
    }
  },
  Category: {
    categories_none: {
      state: 'Excluded'
    }
  },
  Block: {
    blocks_none: {
      state: 'Excluded'
    }
  }
}
export const includedGroupings = {
  Project: {
    projects_some: {
      state: 'Included'
    }
  },
  Context: {
    contexts_some: {
      state: 'Included'
    }
  },
  Mood: {
    moods_some: {
      state: 'Included'
    }
  },
  Category: {
    categories_some: {
      state: 'Included'
    }
  },
  Block: {
    blocks_some: {
      state: 'Included'
    }
  }
}
