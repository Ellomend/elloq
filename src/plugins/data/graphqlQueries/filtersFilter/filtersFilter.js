import {excludedGroupings, includedGroupings} from './filtersQueryConsts'

export const filter = {
  AND: [
    {
      OR: [
        {
          projects_some: {
            state: 'Included'
          }
        },
        {
          contexts_some: {
            state: 'Included'
          }
        },
        {
          blocks_some: {
            state: 'Included'
          }
        },
        {
          categories_some: {
            state: 'Included'
          }
        },
        {
          moods_some: {
            state: 'Included'
          }
        }
      ]
    },
    {
      OR: [
        {
          contexts_none: {
            state: 'Excluded'
          }
        },
        {
          moods_none: {
            state: 'Excluded'
          }
        },
        {
          projects_none: {
            state: 'Excluded'
          }
        },
        {
          categories_none: {
            state: 'Excluded'
          }
        },
        {
          blocks_none: {
            state: 'Excluded'
          }
        }
      ]
    }
  ]
}
const noBlocksFilters = {
  AND: [
    {
      OR: [
        {
          projects_some: {
            state: 'Included'
          }
        },
        {
          contexts_some: {
            state: 'Included'
          }
        },
        {
          categories_some: {
            state: 'Included'
          }
        },
        {
          moods_some: {
            state: 'Included'
          }
        }
      ]
    },
    {
      OR: [
        {
          contexts_none: {
            state: 'Excluded'
          }
        },
        {
          moods_none: {
            state: 'Excluded'
          }
        },
        {
          projects_none: {
            state: 'Excluded'
          }
        },
        {
          categories_none: {
            state: 'Excluded'
          }
        }
      ]
    }
  ]
}

export const timeFilter = function (start) {
  let filter = {
    start_gte: start || null
  }
  if (this.showLate) {
    filter.end_lte = start || null
  }
  return filter
}

export const getPartialFilter = (other) => {
  console.log('other', other)
  let d = new Date()
  let hour = d.getHours()
  let filter = {
    AND: [
      {
        blocks_some: {
          start_lte: hour,
          end_gte: hour
        }
      }
    ]
  }
  if (other) {
    filter.AND = filter.AND.concat(noBlocksFilters.AND)
  }
  return filter
}
export default {
  filter
}

export const getFilterObject = (interGrouping) => {
  let obj = {
    AND: [
      {
      },
      {
      }
    ]
  }
  obj.AND[0][interGrouping] = []
  obj.AND[1][interGrouping] = []

  return obj
}

export const getExcludedGroupingByName = names => {
  let acc = []
  Object.keys(excludedGroupings).forEach(key => {
    if (names.includes(key)) {
      acc.push(excludedGroupings[key])
    }
  })
  return acc
}

export const getIncludedGroupingByName = names => {
  let acc = []
  Object.keys(includedGroupings).forEach(key => {
    if (names.includes(key)) {
      acc.push(includedGroupings[key])
    }
  })
  return acc
}

export const setIncluded = (filterObject, groupingNames, interGrouping) => {
  filterObject.AND[0][interGrouping] = getIncludedGroupingByName(groupingNames)
}

export const setExcluded = (filterObject, groupingNames, interGrouping) => {
  filterObject.AND[1][interGrouping] = getExcludedGroupingByName(groupingNames)
}

export const getGroupingsFilter = (groupingNames, interGrouping = 'OR', excludeOnly = true) => {
  let filterObject = getFilterObject(interGrouping)
  if (!excludeOnly) {
    setIncluded(filterObject, groupingNames, interGrouping)
  }
  setExcluded(filterObject, groupingNames, interGrouping)
  return filterObject
}
