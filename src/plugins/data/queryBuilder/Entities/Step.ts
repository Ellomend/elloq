import GEntity from '../GEntity'

class Step extends GEntity {
    title = 'steps'
    parent = 'goalssteps'
    fields = [
        'id',
        'name',
        'start',
        'end',
        'status',
        'description'
    ]
    attrs = [
        'standardFilters'
    ]

    compile() {
        return `goalssteps: goals {
            ${this.title} (
              ${this.filter.compile()}
            ) {
                ${this.concatFields()}
            }
        }
      `
    }
    pickEntities(res: any, entities: any[]) {
        let steps = res.goalssteps.reduce((acc, goal) => {
            acc = acc.concat(goal.steps)
            return acc
        }, [])
        return entities.concat(steps)
    }
}

export default Step
