import GEntity from '../GEntity'
class Goal extends GEntity {
  title = 'goals'
  fields = [
    'id',
    'name',
    'description'
  ]
  attrs = [
    'standardFilters'
  ]
}
export default Goal
