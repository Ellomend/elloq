import GEntity from '../GEntity'
class Task extends GEntity {
  title = 'tasks'
  fields = [
    'id',
    'name',
    'start',
    'end',
    'status',
    'description'
  ]
  attrs = [
    'standardFilters'
  ]
}
export default Task
