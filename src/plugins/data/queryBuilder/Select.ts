import Rule from './Rule'
import Condition from "./Condition";
import Clause from "./Clause";

export default class Select implements Clause {

    childs: Array<Condition | Rule>;
    private test: string;

    addChild(child: Condition | Rule) {
        this.childs.push(child);
        return this
    }
    add (child: Condition | Rule | Select) {
        let condition = new Condition()
        this.addChild(condition)
        return condition
    }

    constructor(test: string) {
        this.childs = [];
        this.test = test
    }

    concatChilds() {
        return this.childs.reduce((accStr, rule) => {
            accStr += `
                ${rule.compile()}
            `;
            return accStr
        }, ``)
    }

    compile = () => {
        return `
            {
                ${this.test}: [
                    ${this.concatChilds()}
                ]
            }`
    }
}

// makeQuery
//   Tasks
//     dates
//       start
//       end
//       datesKind
//     filters
//       include
//     statuses
//       exclude
//   Logs
//     dates
//       start
//       end
//       datesKind
//     kind
//       include
