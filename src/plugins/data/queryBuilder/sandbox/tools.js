import fs from 'fs'
import path from 'path'
export const dumpData = data => {
  fs.writeFile(path.join(__dirname, '/dump.graphql'), data, function (err) {
    if (err) {
      return console.log(err)
    }

    console.log('The file was saved!')
  })
}
