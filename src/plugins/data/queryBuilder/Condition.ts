import Rule from "./Rule";
import Select from "./Select";
import RulesSet from "./RulesSet";

export default class Condition {
    childs: Array<Rule | Condition | Select | RulesSet>;
    constructor() {
        this.childs = []
    }
    addRule(test: Rule | Condition | Select | RulesSet) {
        this.childs.push(test)
        return this
    }

    /**
     *
     * @returns {string}
     */
    compile (): string {
        let str: string = `
        `;
        str = this.childs.reduce((prev: string, next: Rule | Condition | Select | RulesSet) => {
            prev += next.compile();
            return prev
        }, str)
        str += `
        `
        return str
    }
}