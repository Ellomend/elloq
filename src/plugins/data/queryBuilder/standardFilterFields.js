export default `
                  blocks {
                      id
                      name
                      description
                      start
                      end
                  }
                  categories {
                      id
                      name
                      description
                  }
                  projects {
                      id
                      name
                      description
                  }
                  moods {
                      id
                      name
                      description
                  }
                  contexts {
                      id
                      name
                      description
                  }
`