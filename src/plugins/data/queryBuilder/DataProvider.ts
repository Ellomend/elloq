import Query from "./Query";
import {apolloClient} from '../provider/DataClient/apollo'
import gql from "graphql-tag";

class DataProvider {
    public query: Query;
    apolloClient: apolloClient;
    constructor(userId: string) {
        this.query = new Query(userId)
    }
    addQuery(query: Query) {
        this.query = query
    }

    async fetch() {
        return apolloClient.query({
            query: gql(`${this.query.compile()}`),
            variables: {},
            fetchPolicy: 'network-only'
        })
    }

    async getEntities() {
        let res = await this.fetch()
        res = res.data.User
        let entities = [] as any[]
        this.query.entities.forEach(entity => {
            entities = entity.pickEntities(res, entities)
        })
        return entities
    }
}
export default DataProvider