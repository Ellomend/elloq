import GEntity from "./GEntity";

class Query {
    private readonly userId: string;

    constructor(userId: string) {
        this.userId = userId;
        this.entities = []
    }

    public entities: Array<GEntity>;

    addEntity (entity: GEntity) {
        this.entities.push(entity)
        return this
    }

    get compiledEntities() {
        let string = '';
        string = this.entities.reduce((accStr, entity) => {
            return accStr + entity.compile()
        }, string);
        return string
    }

    compile() {
        return `
    query queryBuilder {
        User (id: "${this.userId}") {
            id
            ${this.compiledEntities}
        }

    }
    `
    }
}

export default Query
