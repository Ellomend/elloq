import Clause from "./Clause";

export default class RawClause implements Clause {
    raw: any

    constructor(raw: any) {
        this.raw = raw || {}
    }

    compile = () => {
        return JSON.stringify(this.raw).replace(/['"]+/g, '')
    }

}