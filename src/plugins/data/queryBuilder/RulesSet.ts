import Clause from "./Clause";
import Condition from "./Condition";
import Rule from "./Rule";

export default class RulesSet implements Clause {
    test: string
    childs: Array<Rule>;
    constructor(test: string) {
        this.test = test
        this.childs = []
    }
    addRule(child: Rule) {
        this.childs.push(child);
        return this
    }
    concatChilds() {
        return this.childs.reduce((accStr, rule) => {
            accStr += `
                ${rule.compile()}
            `;
            return accStr
        }, ``)
    }
    compile = () => {
        return `
            ${this.test}: {
                ${this.concatChilds()}
            }
            `
    }
}