import Clause from "./Clause";

export default class Filter {
    childs: Array<Clause>;

    constructor() {
        this.childs = []
    }

    addClause(child: Clause) {
        this.childs.push(child)
        return this
    }
    concatChilds(): string {
        let str = ``;
        this.childs.forEach(child => {
            str += `
                ${child.compile()}
            `
        });
        return str
    }
    compile(): string {
        let str = `
        filter: {
            AND: [
                    ${this.concatChilds()}
            ]
        }
        `;
        return str
    }
}