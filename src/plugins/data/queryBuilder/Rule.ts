import Clause from "./Clause";

export default class Rule implements Clause {
    private field: string;
    private operation: string;
    private data: string | any;
    private simple: boolean;

    constructor(field: string, operation: string, data: string, simple: boolean = false) {
        this.field = field || 'id';
        this.operation = operation || '';
        this.data = data || null
        this.simple = simple
    }

    get separator() {
        return this.simple
            ? ''
            : '_'
    }

    get getData() {
        if (this.data === null) {
            return 'null'
        } else if (Array.isArray(this.data)) {
            return `[${this.data.map(status => `${status}`)}]`
        } else {
         return `"${this.data}"`
        }
    }

    compile(): string {
        return `{
            ${this.field}${this.separator}${this.operation}: ${this.getData}
        }`
    }
}
