import Filter from "./Filter";

class GEntity {
    title: string;
    fields: Array<string>;
    constructor() {
        this.title = 'tasks';
        this.fields = [];
        this.filter = new Filter();
        // this.conditionsMethods = [];
    }

    public filter: Filter;
    compile() {
      return `
        ${this.title} (
          ${this.filter.compile()}
        ) {
           ${this.concatFields()}
        }
      `
    }
    concatFields() {
        let string: string;
        string = this.fields.reduce((accString, field) => {
            return accString + ',\n' + field
        }, '');
      return string
    }
    setFilter (filter: Filter) {
        this.filter = filter;
        return this
    }

    pickEntities(res: any, entities: any[]) {
        return entities.concat(res[this.title])
    }
}

export default GEntity
