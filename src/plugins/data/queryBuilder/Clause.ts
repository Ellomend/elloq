export default interface Clause {
    compile: () => string
}