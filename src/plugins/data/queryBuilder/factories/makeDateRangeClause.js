import {FILTERS_CONSTS} from '../../../queryBuilder'
import Select from '../Select'
import Condition from '../Condition'
import Rule from '../Rule'

export const kindsMap = new Map([
  [FILTERS_CONSTS.TIME_KINDS.TOUCH_BOTH_SIDES, (start, end) => {
    let clause = new Select('OR')
    clause
      .addChild(new Condition()
        .addRule(new Rule('start', 'lt', end))
        .addRule(new Rule('start', 'gte', start))
        .addRule(new Rule('end', 'gt', end))
      )
      .addChild(new Condition()
        .addRule(new Rule('start', 'gte', start))
        .addRule(new Rule('end', 'lte', end))
      )
      .addChild(new Condition()
        .addRule(new Rule('start', 'lt', start))
        .addRule(new Rule('end', 'gte', start))
        .addRule(new Rule('end', 'lt', end))
      )
    return clause
  }],
  [FILTERS_CONSTS.TIME_KINDS.OUT_FORWARD, (start, end) => {
    let clause = new Condition()
    clause
      .addRule(new Rule('start', 'gt', end))
      .addRule(new Rule('end', 'gt', end))
    return clause
  }],
  [FILTERS_CONSTS.TIME_KINDS.IN_FORWARD, (start, end) => {
    let clause = new Condition()
    clause
      .addRule(new Rule('start', 'lt', end))
      .addRule(new Rule('start', 'gte', start))
      .addRule(new Rule('end', 'gt', end))
    return clause
  }],
  [FILTERS_CONSTS.TIME_KINDS.IN, (start, end) => {
    let clause = new Condition()
    clause
      .addRule(new Rule('start', 'gte', start))
      .addRule(new Rule('end', 'lte', end))
    return clause
  }],
  [FILTERS_CONSTS.TIME_KINDS.IN_BACKWARD, (start, end) => {
    let clause = new Condition()
    clause
      .addRule(new Rule('start', 'lt', start))
      .addRule(new Rule('end', 'gte', start))
      .addRule(new Rule('end', 'lt', end))
    return clause
  }],
  [FILTERS_CONSTS.TIME_KINDS.OUT_BACKWARD, (start, end) => {
    let clause = new Condition()
    clause
      .addRule(new Rule('start', 'lt', start))
      .addRule(new Rule('end', 'lt', start))
    return clause
  }],
  [FILTERS_CONSTS.TIME_KINDS.NONE, (start, end) => {
    return new Condition()
  }]
])

export const makeDateRangeClause = function (kind, start, end, showDateless = true) {
  let Clause = kindsMap.get(kind)(start, end)
  if (showDateless) {
    let dtl = new Select('OR')
      .addChild(
        new Rule('start', '', null, true)
      )
      .addChild(
        new Rule('end', '', null, true)
      )
    dtl.addChild(Clause)
    return dtl
  }
  return Clause
}
