import RawClause from '../RawClause'

export const makeApplyFiltersClause = function (apply) {
  let clause = new RawClause({
    AND: [
      {
        OR: [
          {
            projects_some: {
              state: 'Included'
            }
          },
          {
            contexts_some: {
              state: 'Included'
            }
          },
          {
            blocks_some: {
              state: 'Included'
            }
          },
          {
            categories_some: {
              state: 'Included'
            }
          },
          {
            moods_some: {
              state: 'Included'
            }
          }
        ]
      },
      {
        OR: [
          {
            contexts_none: {
              state: 'Excluded'
            }
          },
          {
            moods_none: {
              state: 'Excluded'
            }
          },
          {
            projects_none: {
              state: 'Excluded'
            }
          },
          {
            categories_none: {
              state: 'Excluded'
            }
          },
          {
            blocks_none: {
              state: 'Excluded'
            }
          }
        ]
      }
    ]
  }
  )
  if (apply) {
    return clause
  }
  return new RawClause({})
}
