import Tasks from './Tasks/Tasks'
import Goals from './Goals/Goals'
import Routines from './Routines'
import Checklists from './Checklists'
import Categories from './Filters/Categories'
import Moods from './Filters/Moods'
import Projects from './Filters/Projects'
import Contexts from './Filters/Contexts'
import Blocks from './Filters/Blocks'
import Custom from './Filters/index'
export const DataModelsRepository = {
  list: {
    Tasks,
    Goals,
    Routines,
    Checklists,
    Categories,
    Moods,
    Projects,
    Contexts,
    Blocks,
    Custom
  },
  model: {
    task: Tasks,
    goal: Goals,
    routine: Routines,
    checklist: Checklists,
    category: Categories,
    mood: Moods,
    project: Projects,
    context: Contexts,
    block: Blocks
  },
  getModel (name) {
    return this.list[name]
  }
}
export default DataModelsRepository
