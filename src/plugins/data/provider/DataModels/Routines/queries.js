import gql from 'graphql-tag'

export const USER_ROUTINES = gql`
    query getUserRoutines (
    $id: ID
    ) {
        User (
            id: $id
        ) {
            routines {
                id
                name
                description
                lastComplete
                schedule
                start
                blocks {
                    id
                    name
                    description
                    start
                    end
                }
                categories {
                    id
                    name
                    description
                }
                projects {
                    id
                    name
                    description
                }
                moods {
                    id
                    name
                    description
                }
                contexts {
                    id
                    name
                    description
                }
            }
        }
    }
`
export const CREATE_ROUTINE = gql`
    mutation createRoutineMutation (
    $name: String!,
    $userId: ID!,
    $description: String,
    $blocksIds: [ID!],
    $categoriesIds: [ID!],
    $projectsIds: [ID!],
    $moodsIds: [ID!],
    $contextsIds: [ID!],
    $lastComplete: DateTime,
    $schedule: String,
    $start: DateTime
    ) {
        createRoutine(
            name: $name,
            userId: $userId,
            description: $description,
            blocksIds: $blocksIds,
            categoriesIds: $categoriesIds,
            projectsIds: $projectsIds,
            moodsIds: $moodsIds,
            contextsIds: $contextsIds,
            lastComplete: $lastComplete,
            schedule: $schedule,
            start: $start
        ) {
            id
            name
            description
        }
    }
`
export const DELETE_ROUTINE = gql`
    mutation deleteRoutineMutation ($id: ID!) {
        deleteRoutine(id: $id) {
            id
        }
    }
`

export const UPDATE_ROUTINE = gql`
    mutation updateRoutineMutation (
    $name: String!,
    $userId: ID!,
    $description: String,
    $id: ID!,
    $categoriesIds: [ID!],
    $projectsIds: [ID!],
    $blocksIds: [ID!],
    $moodsIds: [ID!],
    $contextsIds: [ID!],
    $lastComplete: DateTime,
    $schedule: String
    $start: DateTime
    ) {
        updateRoutine(
            name: $name,
            userId: $userId,
            description: $description,
            id: $id,
            categoriesIds: $categoriesIds,
            projectsIds: $projectsIds,
            blocksIds: $blocksIds,
            moodsIds: $moodsIds,
            contextsIds: $contextsIds,
            lastComplete: $lastComplete,
            schedule: $schedule,
            start: $start
        ) {
            id
            name
            description
        }
    }
`

export const UPDATE_ROUTINE_WITH_OBJECTS = gql`
    mutation updateRoutineWithObjectsMutation (
    $name: String!,
    $userId: ID!,
    $description: String,
    $id: ID!,
    $categories: [RoutinecategoriesCategory!],
    $projects: [RoutineprojectsProject!],
    $blocks: [RoutineblocksBlock!],
    $moods: [RoutinemoodsMood!],
    $contexts: [RoutinecontextsContext!],
    $lastComplete: DateTime,
    $schedule: String,
    $start: DateTime
    ) {
        updateRoutine(
            name: $name,
            userId: $userId,
            description: $description,
            id: $id,
            categories: $categories,
            projects: $projects,
            blocks: $blocks,
            moods: $moods,
            contexts: $contexts,
            lastComplete: $lastComplete,
            schedule: $schedule,
            start: $start
        ) {
            id
            name
            description
        }
    }
`

export const GET_ROUTINE_BY_ID = gql`
    query getRoutineById (
    $id: ID
    ) {
        Routine (
            id: $id
        ) {

            id
            name
            description
            lastComplete
            schedule
            start
            blocks {
                id
                name
                description
                start
                end
            }
            categories {
                id
                name
                description
            }
            projects {
                id
                name
                description
            }
            moods {
                id
                name
                description
            }
            contexts {
                id
                name
                description
            }
        }
    }

`

export const UPDATE_ROUTINE_SCHEDULE = gql`
    mutation updateRoutineScheduleMutation (
    $id: ID!,
    $schedule: String
    ) {
        updateRoutine(
            id: $id,
            schedule: $schedule,
        ) {
            id
            name
            description
            schedule
        }
    }
`
export const UPDATE_ROUTINE_START = gql`
    mutation updateRoutineStartMutation (
    $id: ID!,
    $start: DateTime
    ) {
        updateRoutine(
            id: $id,
            start: $start,
        ) {
            id
            name
            description
            start
        }
    }
`
export const UPDATE_ROUTINE_LASTCOMPLETE = gql`
    mutation updateRoutineLastCompleteMutation (
    $id: ID!,
    $lastComplete: DateTime
    ) {
        updateRoutine(
            id: $id,
            lastComplete: $lastComplete,
        ) {
            id
            name
            description
            lastComplete
        }
    }
`
