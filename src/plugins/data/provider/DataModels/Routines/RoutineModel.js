import BaseModel from '@/data/provider/DataModels/BaseModel/BaseModel'
import { FILTERSATTR } from '@/data/provider/DataModels/Constants'
import routineMethods from './methods'
export const RoutineModel = {
  ...BaseModel,
  ...routineMethods,
  modelName: 'routine',
  modelMethods: {
  },
  modelAttributes: {
  },
  fieldAttributes: [
    FILTERSATTR
  ]
}
export default RoutineModel
