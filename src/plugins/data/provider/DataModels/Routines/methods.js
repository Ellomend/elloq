export const belongsToDay = function (day) {
  return false
}
export const getStatus = function () {
  if (!this.hasStart() || !this.hasLastComplete()) {
    return false
  }
  return true
}
export const hasStart = function () {
  return !!this.start
}
export const hasLastComplete = function () {
  return !!this.lastComplete
}

export default {
  belongsToDay,
  getStatus,
  hasLastComplete,
  hasStart
}
