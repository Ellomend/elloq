import gql from 'graphql-tag'

export const USER_PROJECTS = gql`
    query getUserProjects ($id: ID){
        User (id: $id) {
            projects {
                id
                name
                description
            }
        }
    }
`
export const CREATE_PROJECT = gql`
    mutation createProjectMutation (
    $name: String!,
    $userId: ID!,
    $description: String
    ) {
        createProject(
            name: $name,
            userId: $userId,
            description: $description
        ) {
            id
            name
            description
        }
    }
`
export const DELETE_PROJECT = gql`
    mutation deleteProjectMutation ($id: ID!) {
        deleteProject(id: $id) {
            id
        }
    }
`
export const UPDATE_PROJECT = gql`
    mutation updateProjectMutation (
    $name: String!,
    $userId: ID!,
    $description: String,
    $id: ID!
    ) {
        updateProject(
            name: $name,
            userId: $userId,
            description: $description,
            id: $id,
        ) {
            id
            name
            description
        }
    }
`
