import BaseModel from '@/data/provider/DataModels/BaseModel/BaseModel'

export const fieldAttributes = [
]
const hasFieldAttribute = function (field) {
  return this.fieldAttributes.includes(field)
}
export default {
  ...BaseModel,
  fieldAttributes,
  hasFieldAttribute
}
