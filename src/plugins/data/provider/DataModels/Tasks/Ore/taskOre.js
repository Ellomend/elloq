import gql from 'graphql-tag'
import Client from '../../../DataClient/DataClient'
import {getUserFromStorage} from '../../../../../config'

export const getUserId = function () {
  let userData = getUserFromStorage()
  let userId = null
  if (userData.user) {
    userId = userData.user.id
  }
  console.log('from ls')
  console.log(userData)
  console.log(userId)
  return userId
}
export const taskAttributes = 'id\n                  name\n                  start\n                  end\n                  status\n                  description\n                  blocks {\n                      id\n                      name\n                      description\n                      start\n                      end\n                  }\n                  categories {\n                      id\n                      name\n                      description\n                  }\n                  projects {\n                      id\n                      name\n                      description\n                  }\n                  moods {\n                      id\n                      name\n                      description\n                  }\n                  contexts {\n                      id\n                      name\n                      description\n                  }'
export let q = gql`
    query getLateTasks ($userId: ID!, $start: DateTime, $end: DateTime)
    {
        User (id: $userId) {
            name
            tasks (
                filter: {
                    start_lt: $start
                    end_lt: $end,
                    status_not_in:[Complete, Failed, Cancelled]
                }
            ) {
                ${taskAttributes}
            }
        }

    }

`
export default {
  ore: 'Task',
  query: q,
  req: function (start, end) {
    let userId = getUserId()
    console.log('uid')
    console.log(userId)
    return Client.query({
      query: this.query,
      variables: {
        userId: getUserId(),
        start: start,
        end: end
      },
      fetchPolicy: 'network-only'
    })
  }
}
