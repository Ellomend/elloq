// functions used specifically for tasks
import moment from 'moment'

/**
 * returns object with start and end variables
 * @param task
 * @returns {{start, end}}
 */
const getDatesRange = function () {
  return {
    start: this.start,
    end: this.end
  }
}
/**
 * hydrates task with methods
 * @param task
 * @returns {{getDatesRange: getDatesRange, datesIn: Array}}
 */
export const taskFactory = function (task) {
  let newTask = {
    ...task,
    belongsToDay,
    ...taskFunctions,
    type: 'task'
  }
  return newTask
}
/**
 * hydrates array of tasks
 * @param tasks
 * @returns Array tasks
 */
export const tasksFactory = function (tasks) {
  let tasksCollection = tasks.map(task => {
    return taskFactory(task)
  })
  return tasksCollection
}

export const belongsToDay = function (day) {
  if (!this.start || !this.end) {
    return false
  }
  let dayTest = moment(day.string)
  return dayTest.isBetween(moment(this.start), moment(this.end), null, '[]')
}
function getStartDate () {
  if (!this.start || !this.end) {
    return false
  }
  return moment(this.start)
}
function getEndDate () {
  if (!this.start || !this.end) {
    return false
  }
  return moment(this.end)
}

function hasValidDates () {
  return (this.start && this.end)
}
export const taskFunctions = {
  getDatesRange,
  getStartDate,
  getEndDate,
  hasValidDates,
  belongsToDay
}
export default taskFunctions
