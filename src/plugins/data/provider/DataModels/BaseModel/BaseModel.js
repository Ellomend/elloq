import BaseMethods from './BaseMethods'
export const getType = function () {
  return this.type
}
export const BaseModel = {
  modelName: 'Base',
  modelMethods: {
  },
  modelAttributes: {},
  ...BaseMethods,
  getType
}
export default BaseModel
