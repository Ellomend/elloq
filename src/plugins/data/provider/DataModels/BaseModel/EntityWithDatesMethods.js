import moment from 'moment/moment'

export const belongsToDay = function (day) {
  if (!this.start || !this.end) {
    return false
  }
  let dayTest = moment(day.string)
  return dayTest.isBetween(moment(this.start), moment(this.end), null, '[]')
}

export default {
  belongsToDay
}
