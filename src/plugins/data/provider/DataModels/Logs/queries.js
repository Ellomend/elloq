import gql from 'graphql-tag'

export const USER_LOGS = gql`
    query getUserLogs ($id: ID){
        User (id: $id) {
            logs {
                id
                type
                kind
                comment
                time
            }
        }
    }
`
export const CREATE_LOG = gql`
    mutation createLogMutation (
        $userId: ID!
        $type: String!
        $kind: String!
        $comment: String
        $time: DateTime!
    ) {
        createLog(
            userId: $userId,
            type: $type,
            kind: $kind,
            comment: $comment
            time: $time
        ) {
            id
            kind
            type
            comment
            time
        }
    }
`
export const DELETE_LOG = gql`
    mutation deleteLogMutation ($id: ID!) {
        deleteLog(id: $id) {
            id
        }
    }
`
export const UPDATE_LOG = gql`
    mutation updateLogMutation (
        $id: ID!
        $type: String!
        $kind: String!
        $comment: String
        $time: DateTime!
    ) {
        updateLog(
            id: $id,
            type: $type,
            kind: $kind,
            comment: $comment
            time: $time
        ) {
            id
            comment
            kind
            time 
            type 
        }
    }
`
