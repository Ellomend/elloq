import Client from '@/data/provider/DataClient/DataClient'
import {CREATE_LOG, DELETE_LOG, UPDATE_LOG, USER_LOGS} from './queries'
import { getUserFromStorage } from '@/config'
export const Logs = {
  async fetchList () {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.query({
      query: USER_LOGS,
      variables: {
        id: userId
      },
      fetchPolicy: 'network-only'
    })
    return resp.data.User.logs
  },
  async add (payload) {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.mutate({
      mutation: CREATE_LOG,
      variables: {
        userId: userId,
        ...payload
      }
    })
    return resp
  },
  async remove (payload) {
    let resp = await Client.mutate({
      mutation: DELETE_LOG,
      variables: {
        id: payload.id
      }
    })
    return resp
  },
  async update (payload) {
    let resp = await Client.mutate({
      mutation: UPDATE_LOG,
      variables: {
        ...payload
      }
    })
    return resp
  },
  convertDateConstrainType (type) {
    return type
      .replace('end', 'time')
      .replace('start', 'time')
  }
}
export default Logs
