import Client from '@/data/provider/DataClient/DataClient'
import { getUserFromStorage } from '@/config'
import {
  CREATE_CHECKLIST, DELETE_CHECKLIST, UPDATE_CHECKLIST,
  USER_CHECKLISTS, CLONE_CHECKLIST, SET_POINT_STATUS,
  GET_CHECKLIST_BY_ID
} from '@/data/provider/DataModels/Checklists/queries'
import { modelsFactory } from '../modelsFactory'
import { ChecklistModel } from '@/data/provider/DataModels/Checklists/ChecklistModel'
import PointModel from '@/data/provider/DataModels/Checklists/PointModel'
import {convertRelationsToIds} from '@/data/provider/DataModels/modelsFactory'
import {orderPointByOrder} from '@/data/provider/DataModels/Checklists/methods'
import {cloneDeep} from 'lodash'

export const Checklists = {
  name: 'Checklists',
  async fetchList () {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.query({
      query: USER_CHECKLISTS,
      variables: {
        id: userId
      },
      fetchPolicy: 'network-only'
    })
    let checklists = resp.data.User.checklists.map(checklist => {
      checklist = {...checklist}
      checklist = orderPointByOrder(checklist)
      checklist.points = modelsFactory(checklist.points, PointModel)
      return checklist
    })
    return modelsFactory(checklists, ChecklistModel)
  },
  async getChecklistById (id) {
    let resp = await Client.query({
      query: GET_CHECKLIST_BY_ID,
      variables: {
        id: id
      },
      fetchPolicy: 'network-only'
    })
    let checklists = [resp.data.Checklist].map(checklist => {
      checklist = {...checklist}
      checklist = orderPointByOrder(checklist)
      checklist.points = modelsFactory(checklist.points, PointModel)
      return checklist
    })
    let checklist = checklists[0]
    checklist = modelsFactory([checklist], ChecklistModel)
    return checklist[0]
  },
  async add (payload) {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.mutate({
      mutation: CREATE_CHECKLIST,
      variables: {
        name: payload.name,
        type: payload.type || 0,
        userId: userId,
        description: payload.description,
        ...convertRelationsToIds(payload)
      }
    })
    return resp
  },
  async remove (payload) {
    let resp = await Client.mutate({
      mutation: DELETE_CHECKLIST,
      variables: {
        id: payload.id
      }
    })
    return resp
  },
  async update (payload) {
    let resp = await Client.mutate({
      mutation: UPDATE_CHECKLIST,
      variables: {
        name: payload.name,
        description: payload.description,
        id: payload.id,
        ...convertRelationsToIds(payload),
        type: payload.type || 0
      }
    })
    return resp
  },
  async startChecklist (checklist) {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let newChecklist = cloneDeep(checklist)
    delete newChecklist.id
    newChecklist.points = newChecklist.points.map(point => {
      point = cloneDeep(point)
      delete point.id
      delete point.checklist
      delete point.__typename
      return point
    })
    newChecklist.blocksIds = newChecklist.blocks.map(block => {
      return block.id
    })
    newChecklist.contextsIds = newChecklist.contexts.map(context => {
      return context.id
    })
    newChecklist.projectsIds = newChecklist.projects.map(project => {
      return project.id
    })
    newChecklist.categoriesIds = newChecklist.categories.map(category => {
      return category.id
    })
    newChecklist.moodsIds = newChecklist.moods.map(mood => {
      return mood.id
    })
    newChecklist.type = 1
    let points = newChecklist.points
    console.log(points)
    debugger
    let resp = await Client.mutate({
      mutation: CLONE_CHECKLIST,
      variables: {
        name: newChecklist.name,
        description: newChecklist.description,
        points: points,
        type: newChecklist.type,
        blocksIds: newChecklist.blocksIds,
        contextsIds: newChecklist.contextsIds,
        projectsIds: newChecklist.projectsIds,
        categoriesIds: newChecklist.categoriesIds,
        moodsIds: newChecklist.moodsIds,
        userId: userId
      }
    })
    return resp
  },
  async setPointStatus (pointId, status) {
    let resp = await Client.mutate({
      mutation: SET_POINT_STATUS,
      variables: {
        id: pointId,
        status: status
      }
    })
    return resp
  }
}
export default Checklists
