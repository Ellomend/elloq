import EntityMethods from '@/data/provider/DataModels/BaseModel/EntityMethods'
import BaseMethods from '@/data/provider/DataModels/BaseModel/BaseMethods'
import { STATUSATTR } from '@/data/provider/DataModels/Constants'
import BaseModel from '@/data/provider/DataModels/BaseModel/BaseModel'

export const PointModel = {
  ...BaseModel,
  modelName: 'point',
  ...BaseMethods,
  ...EntityMethods,
  modelMethods: {
  },
  modelAttributes: {},
  fieldAttributes: [
    STATUSATTR
  ]
}
export default PointModel
