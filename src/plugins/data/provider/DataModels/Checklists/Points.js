import Client from '@/data/provider/DataClient/DataClient'
import { getUserFromStorage } from '@/config'
import {
  ADD_POINT,
  CHECKLISTS_POINTS, REMOVE_POINT, GET_POINT_BY_ID, UPDATE_POINT, USER_POINTS
  } from '@/data/provider/DataModels/Checklists/queries'
import { modelsFactory } from '../modelsFactory'
import PointModel from '@/data/provider/DataModels/Checklists/PointModel'

export const Points = {
  async fetchChecklistPoints (checklistId) {
    let resp = await Client.query({
      query: CHECKLISTS_POINTS,
      variables: {
        id: checklistId
      },
      fetchPolicy: 'network-only'
    })
    return modelsFactory(resp.data.Checklist.points, PointModel)
  },
  async getPoints () {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.query({
      query: USER_POINTS,
      variables: {
        id: userId
      },
      fetchPolicy: 'network-only'
    })
    return modelsFactory(resp.data.User.checklists.points, PointModel)
  },
  async add (point) {
    let resp = await Client.mutate({
      mutation: ADD_POINT,
      variables: {
        ...point
      }
    })
    return resp.data
  },
  async remove (point) {
    let resp = await Client.mutate({
      mutation: REMOVE_POINT,
      variables: {
        id: point.id
      }
    })
    return resp
  },
  async getPointById (id) {
    let resp = await Client.query({
      query: GET_POINT_BY_ID,
      variables: {
        id: id
      }
    })
    return resp.data.Point
  },
  async update (point) {
    let resp = await Client.mutate({
      mutation: UPDATE_POINT,
      variables: {
        ...point
      }
    })
    return resp
  },
  pointUp (point) {
    point.order = point.order + 1
    return this.updatePoint(point)
  },
  pointDown (point) {
    point.order = point.order - 1
    return this.updatePoint(point)
  }
}
export default Points
