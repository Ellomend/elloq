import BaseModel from '@/data/provider/DataModels/BaseModel/BaseModel'
import { CHILDRENSATTR, DATESATTR, FILTERSATTR } from '@/data/provider/DataModels/Constants'
import EntityWithDatesMethods from '@/data/provider/DataModels/BaseModel/EntityWithDatesMethods'

export const GoalModel = {
  ...BaseModel,
  ...EntityWithDatesMethods,
  modelName: 'goal',
  modelMethods: {
  },
  modelAttributes: {
  },
  fieldAttributes: [
    CHILDRENSATTR,
    FILTERSATTR,
    DATESATTR
  ]
}
export default GoalModel
