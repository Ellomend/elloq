import Client from '@/data/provider/DataClient/DataClient'
// import { getUserFromStorage } from '@/config'
import {
  CREATE_STEP, GET_STEP_BY_ID, GOAL_STEPS, REMOVE_STEP, UPDATE_STEP
} from '@/data/provider/DataModels/Goals/queries'
import { modelsFactory } from '@/data/provider/DataModels/modelsFactory'
import StepModel from '@/data/provider/DataModels/Goals/StepModel'

export const Steps = {
  async fetchSteps (goal) {
    let resp = await Client.query({
      query: GOAL_STEPS,
      variables: {
        id: goal
      },
      fetchPolicy: 'network-only'
    })
    return modelsFactory(resp.data.Goal.steps, StepModel)
  },
  async getStepById (id) {
    let resp = await Client.query({
      query: GET_STEP_BY_ID,
      variables: {
        id: id
      }
    })
    return modelsFactory([resp.data.Step], StepModel)[0]
  },
  async add (payload) {
    console.log('create step', payload)
    let resp = await Client.mutate({
      mutation: CREATE_STEP,
      variables: {
        name: payload.name,
        description: payload.description,
        start: payload.start,
        end: payload.end,
        goalId: payload.goalId,
        status: payload.status
      }
    })
    return resp.data.createStep
  },
  async remove (payload) {
    console.log('remove step id', payload.id)
    let resp = await Client.mutate({
      mutation: REMOVE_STEP,
      variables: {
        id: payload.id
      }
    })
    return resp.data.deleteStep
  },
  async update (payload) {
    let resp = await Client.mutate({
      mutation: UPDATE_STEP,
      variables: {
        stepId: payload.id,
        name: payload.name,
        description: payload.description,
        status: payload.status,
        start: payload.start,
        end: payload.end,
        finished: payload.finished
      },
      fetchPolicy: 'no-cache'
    })
    return resp.data.updateStep
  }
}
export default Steps
