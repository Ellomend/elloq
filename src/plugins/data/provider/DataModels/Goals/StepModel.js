import BaseModel from '@/data/provider/DataModels/BaseModel/BaseModel'
import {DATESATTR, ISENTITYATTR, STATUSATTR} from '@/data/provider/DataModels/Constants'
import EntityMethods from '@/data/provider/DataModels/BaseModel/EntityMethods'
import EntityWithDatesMethods from '@/data/provider/DataModels/BaseModel/EntityWithDatesMethods'

export const StepModel = {
  ...BaseModel,
  ...EntityMethods,
  ...EntityWithDatesMethods,
  modelName: 'step',
  modelMethods: {
  },
  modelAttributes: {
  },
  fieldAttributes: [
    ISENTITYATTR,
    DATESATTR,
    STATUSATTR
  ]
}
export default StepModel
