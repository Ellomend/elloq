import Client from '@/data/provider/DataClient/DataClient'
import { getUserFromStorage } from '@/config'
import {
  CREATE_GOAL, DELETE_GOAL, UPDATE_GOAL,
  USER_GOALS
} from '@/data/provider/DataModels/Goals/queries'
import {convertRelationsToIds, modelsFactory} from '@/data/provider/DataModels/modelsFactory'
import GoalModel from '@/data/provider/DataModels/Goals/GoalModel'
import StepModel from '@/data/provider/DataModels/Goals/StepModel'

export const Goals = {
  name: 'Goals',

  async fetchList () {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.query({
      query: USER_GOALS,
      variables: {
        id: userId
      },
      fetchPolicy: 'network-only'
    })
    let goals = resp.data.User.goals.map(goal => {
      goal = {...goal}
      goal.steps = modelsFactory(goal.steps, StepModel)
      return goal
    })
    return modelsFactory(goals, GoalModel)
  },
  async add (payload) {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.mutate({
      mutation: CREATE_GOAL,
      variables: {
        name: payload.name,
        userId: userId,
        description: payload.description,
        start: payload.start,
        end: payload.end,
        ...convertRelationsToIds(payload)
      }
    })
    return resp
  },
  async remove (payload) {
    let resp = await Client.mutate({
      mutation: DELETE_GOAL,
      variables: {
        id: payload.id
      }
    })
    return resp
  },
  async update (payload) {
    let userData = getUserFromStorage()
    let userId = null
    if (userData.user) {
      userId = userData.user.id
    }
    let resp = await Client.mutate({
      mutation: UPDATE_GOAL,
      variables: {
        name: payload.name,
        userId: userId,
        description: payload.description,
        id: payload.id,
        start: payload.start,
        end: payload.end,
        ...convertRelationsToIds(payload)
      }
    })
    return resp
  }
}
export default Goals
