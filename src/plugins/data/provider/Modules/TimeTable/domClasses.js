import moment from 'moment'

export const domClasses = {
  calculateWeekStyle (start, end) {
    let startLine = moment(start).isoWeekday()
    let endLine = moment(end).isoWeekday() + 1
    let style = {
      'grid-column-start': startLine,
      'grid-column-end': endLine
    }
    return style
  },
  calculateByRange (start, end, entity) {
    let startLine = moment(start).isoWeekday()
    let endLine = moment(end).isoWeekday() + 1
    let style = {
      'grid-column-start': startLine,
      'grid-column-end': endLine
    }
    return style
  }
}
export default domClasses
