/**
 * Created by Alex on 26.01.2018.
 * This kinda pile of functions to just store them
 */

import moment from 'moment'

export const functions = {
  name: 'functions'
}

export const getThisMonthDays = function () {
  let start = moment().startOf('month')
  let end = moment().endOf('month')
  console.log('start, end', start, end)
  return getRangeDays(start, end)
}
export const getRangeDays = function (start, end) {
  if (!start || !end) {
    return []
  }
  return getRangeOfDates(start, end, 'day')
}
export const getRangeOfDates = function (start, end, key, arr = [start.startOf(key)]) {
  if (start.isAfter(end)) throw new Error('start must precede end')

  const next = moment(start).add(1, key).startOf(key)

  if (next.isAfter(end, key)) return arr
  return getRangeOfDates(next, end, key, arr.concat(next))
}
/**
 * adds row to days array
 * @param entities
 * @param name
 */
export const addRow = function (entities, name) {
  if (!entities || !name || !entities.length) {
    return
  }
  this.map(day => {
    day.lists[name] = getEntitiesForDate(day, entities)
    return day
  })
}
/**
 * getting entities for dayModel
 * @param day
 * @param entities
 * @returns {Array}
 */
export const getEntitiesForDate = function (day, entities) {
  return filterEntitiesByDate(day, entities)
}
export const filterEntitiesByDate = function (day, entities) {
  if (!entities) {
    return []
  }
  let day1 = day
  let fitEntities = entities.filter(entity => {
    return entity.belongsToDay(day1)
  })
  return fitEntities
}
export const getThisWeekDays = function () {
  let monday = moment().startOf('isoWeek')
  let sunday = moment().endOf('isoWeek')
  return getRangeDays(monday, sunday)
}
