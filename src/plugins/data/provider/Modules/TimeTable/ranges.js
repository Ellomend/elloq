import {
  getRangeDays, getRangeOfDates, getThisMonthDays,
  getThisWeekDays
} from '@/data/provider/Modules/TimeTable/functions'

export const ranges = {
  getThisMonthDays,
  getThisWeekDays,
  getRangeDays,
  getRangeOfDates
}
export default ranges
