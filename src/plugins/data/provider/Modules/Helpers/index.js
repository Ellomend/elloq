import * as funs from '@/data/provider/Modules/Helpers/helpersFunctions'

export default {
  name: 'helpers',
  ...funs
}
