const typesToRels = {
  block: 'blocks',
  mood: 'moods',
  project: 'projects',
  context: 'contexts',
  category: 'categories'
}

function isEntityHaveFilter (filter, entity) {
  // here we want to iterate thru relation of entity to see if id matches
  let rels = entity[typesToRels[filter.type]]
  let res = rels.find(rel => {
    return rel.id === filter.id
  })
  return Boolean(res)
}

function isEntityHaveAllFilters (filtersArray, entity) {
  // we have pile of filters, lets check every single one
  let have = true
  for (let filter of filtersArray) {
    if (isEntityHaveFilter(filter, entity)) {
      have = false
    }
  }
  return have
}

function isEntityDontHaveAllFilters (onlyHasNotArray, entity) {
  let donthave = true
  for (let filter of onlyHasNotArray) {
    if (!isEntityHaveFilter(filter, entity)) {
      donthave = false
    }
  }
  return donthave
}

const filterByStoreFilters = function (options, entity) {
  let result = true
  // first, check if onlyHas is not empty
  let onlyHasArray = options.filter(item => {
    return item.constrain === true
  })
  // if that the case we don't want to show entity that not contain this related filter
  if (onlyHasArray && onlyHasArray.length > 0) {
    result = isEntityHaveAllFilters(onlyHasArray, entity)
  }
  // now we want to check if entity dont have any of onlyHasNot filters
  let onlyHasNotArray = options.filter(item => {
    return item.constrain === false
  })
  // if that the case we don't want to show entity that not contain this related filter
  if (onlyHasNotArray && onlyHasNotArray.length > 0) {
    result = isEntityDontHaveAllFilters(onlyHasNotArray, entity)
  }
  return result
}

const filterByFilters = function (options) {
  return {
    options,
    execute (entity) {
      return filterByStoreFilters(options, entity)
    }
  }
}
export default filterByFilters
