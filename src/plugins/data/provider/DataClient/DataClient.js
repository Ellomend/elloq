import apolloClient from './apollo'
export const DataClient = {
  name: 'DataClient',
  client: apolloClient
}
export default DataClient.client
