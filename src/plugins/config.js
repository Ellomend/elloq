import store from '../store'

export const getUserFromStorage = function () {
  console.log('getting user', store.state.user.user.id)
  return {
    user: {
      id: store.state.user.user.id
    }
  }
}
export default {
  getUserFromStorage
}
