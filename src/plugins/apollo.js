
import VueApollo from 'vue-apollo'
import providerClient from './data/provider/DataClient/apollo'

export default ({app, router, Vue}) => {
  const apolloProvider = new VueApollo({
    defaultClient: providerClient
  })
  Vue.use(VueApollo)
  app.provide = apolloProvider.provide()
  Vue.prototype.$client = apolloProvider
}
