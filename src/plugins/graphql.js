// import something here
import gql from 'graphql-tag'
import customQueries from './data/graphqlQueries'
import { filter } from './data/graphqlQueries/filtersFilter/filtersFilter'
export const stuff = {
  customQueries,
  // register
  //      query createUser(authProvider: { email: { email, password } })
  REGISTER_MUTATION: gql`
      mutation register ($name: String!, $email: String!, $password: String!){
          createUser (name: $name, authProvider: {
              email: {
                  email: $email,
                  password: $password
              }
          } ) {
              id
          }
      }
  `,
  // login
  LOGIN_MUTATION: gql`
      mutation SigninUserMutation($email: String!, $password: String!) {
          signinUser(email: {
              email: $email,
              password: $password
          }) {
              token
              user {
                  id
                  email
              }
          }
      }
  `,
  // create task
  ALL_ENTITIES_QUERY: gql`
    query getAllEntities (
        $userId: ID!,
        $checklistFilter: ChecklistFilter,
        $goalFilter: GoalFilter,
        $taskFilter: TaskFilter,
        $stepFilter: StepFilter,
        $pointFilter: PointFilter,
        $logFilter: LogFilter
    )
      {
          User (id: $userId) {
              name
              logs (filter: $logFilter) {
                  id
                  type
                  kind
                  comment
                  time
              }
              checklists (filter: $checklistFilter) {
                  id
                  name
                  description
                  type
                  blocks {
                      id
                      name
                      description
                      start
                      end
                  }
                  categories {
                      id
                      name
                      description
                  }
                  projects {
                      id
                      name
                      description
                  }
                  moods {
                      id
                      name
                      description
                  }
                  contexts {
                      id
                      name
                      description
                  }
                  points (filter: $pointFilter) {
                      id
                      name
                      description
                      order
                      status
                      checklist {
                          id
                          name
                          description
                          type
                      }
                  }
              }
              goals (filter: $goalFilter) {
                  id
                  name
                  description
                  start
                  end
                  steps (
                      filter: $stepFilter
                  ) {
                      id
                      name
                      description
                      start
                      end
                      status
                      goal {
                          id
                      }
                  }
                  blocks {
                      id
                      name
                      description
                      start
                      end
                  }
                  categories {
                      id
                      name
                      description
                  }
                  projects {
                      id
                      name
                      description
                  }
                  moods {
                      id
                      name
                      description
                  }
                  contexts {
                      id
                      name
                      description
                  }
              }
              goalssteps: goals {
                  steps (
                      filter: $stepFilter
                  ) {
                      id
                      name
                      description
                      start
                      end
                      status
                      goal {
                          id
                      }
                  }
              }
              tasks (filter: $taskFilter) {
                  id
                  name
                  start
                  end
                  status
                  description
                  blocks {
                      id
                      name
                      description
                      start
                      end
                  }
                  categories {
                      id
                      name
                      description
                  }
                  projects {
                      id
                      name
                      description
                  }
                  moods {
                      id
                      name
                      description
                  }
                  contexts {
                      id
                      name
                      description
                  }
              }
          }

      }
  `,
  CHECKLISTS_QUERY: gql`
    query getChecklists (
        $userId: ID!,
        $checklistFilter: ChecklistFilter
    )
    {
        User (id: $userId) {
            checklists (filter: $checklistFilter) {
                id
                name
                description
                type
                blocks {
                    id
                    name
                    description
                    start
                    end
                }
                categories {
                    id
                    name
                    description
                }
                projects {
                    id
                    name
                    description
                }
                moods {
                    id
                    name
                    description
                }
                contexts {
                    id
                    name
                    description
                }
                points {
                    id
                    name
                    description
                    order
                    status
                    checklist {
                        id
                        name
                        description
                        type
                    }
                }
            }
        }
    }
  `,
  ALL_FILTERS_QUERY: gql`
      query allUserFiltersQuery ($id:ID) {
          User (id: $id) {
              blocks {
                  name
                  description
                  start
                  end
                  id
                  state
              }
              categories {
                  name
                  description
                  id
                  state
              }
              moods {
                  id
                  name
                  description
                  state
              }
              projects {
                  id
                  name
                  description
                  state
              }
              contexts {
                  id
                  name
                  description
                  color
                  state
              }
          }
      }
  `,
  CATEGORIES_LIST: gql`
      query allUsersCategories ($id:ID) {
          User (id: $id) {
              categories {
                  name
                  description
                  id
                  state
                  checklists {
                      id
                      name
                      description
                      type
                      points {
                          id
                          name
                          description
                          order
                          status
                          checklist {
                              id
                              name
                              description
                              type
                          }
                      }
                      blocks {
                          name
                          description
                          start
                          end
                          id
                          state
                      }
                      categories {
                          name
                          description
                          id
                          state
                      }
                      moods {
                          id
                          name
                          description
                          state
                      }
                      projects {
                          id
                          name
                          description
                          state
                      }
                      contexts {
                          id
                          name
                          description
                          color
                          state
                      }
                  }
                  tasks {
                      id
                      name
                      start
                      end
                      status
                      description
                      blocks {
                          name
                          description
                          start
                          end
                          id
                          state
                      }
                      categories {
                          name
                          description
                          id
                          state
                      }
                      moods {
                          id
                          name
                          description
                          state
                      }
                      projects {
                          id
                          name
                          description
                          state
                      }
                      contexts {
                          id
                          name
                          description
                          color
                          state
                      }
                  }
                  goals {
                      id
                      name
                      description
                      start
                      end
                      steps {
                          id
                          name
                          description
                          start
                          end
                          status
                          goal {
                              id
                          }
                      }
                      blocks {
                          name
                          description
                          start
                          end
                          id
                          state
                      }
                      categories {
                          name
                          description
                          id
                          state
                      }
                      moods {
                          id
                          name
                          description
                          state
                      }
                      projects {
                          id
                          name
                          description
                          state
                      }
                      contexts {
                          id
                          name
                          description
                          color
                          state
                      }
                  }
              }
          }
      }
  `,
  applyFilter: filter,
  notApplyFilter: '{}'
}

// leave the export, even if you don't use it
export default ({app, router, Vue}) => {
  // something to do
  Vue.prototype.$graphql = stuff
}
