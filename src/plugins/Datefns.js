// import something here
import dfs from 'date-fns'

// leave the export, even if you don't use it
export default ({ app, router, Vue }) => {
  Vue.prototype.$dfs = dfs
  Vue.mixin({
    methods: {
      todayEnd () {
        return dfs.endOfDay(Date.now())
      },
      todayStart () {
        return dfs.startOfDay(Date.now())
      }
    }
  })
}
