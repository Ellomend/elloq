// import something here

// leave the export, even if you don't use it
import Provider from './data/ProviderPlugin'

export default ({ app, router, Vue }) => {
  Vue.use(Provider)
}
