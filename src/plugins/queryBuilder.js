export const FILTERS_CONSTS = {
  TIME_KINDS: {
    SELF: 'datesKind',
    TOUCH_BOTH_SIDES: 'touch_both_sides',
    OUT_FORWARD: 'out_forward',
    IN_FORWARD: 'in_forward',
    IN: 'in',
    IN_BACKWARD: 'in_backward',
    OUT_BACKWARD: 'out_backward',
    NONE: 'none'
  }
}
export default ({ app, router, Vue }) => {
  Vue.mixin({
    computed: {
      getConditionOptions () {
        return {
          entities: [],
          filters: {
            apply: false
          },
          dates: {
            start: '',
            end: '',
            timeKind: null,
            applyDatesFilter: false,
            showDateless: ''
          },
          statuses: {
            include: []
          }
        }
      }
    }
  })
}
