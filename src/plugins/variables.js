
const entityTypeIcons = {
  'Task': 'crop_landscape',
  'Point': 'done',
  'Step': 'call_made',
  'Goal': 'trending_up',
  'Checklist': 'format_list_numbered',
  'Log': 'create'
}

const filterTypeIcons = {
  'Block': 'alarm_on',
  'Category': 'code',
  'Mood': 'accessibility_new',
  'Project': 'assignment',
  'Context': 'loyalty'
}
let statusSettings = {
  'Pending': {
    icon: 'play_circle_outline',
    color: 'blue-6',
    label: 'Pending'
  },
  'Working': {
    icon: 'create',
    color: 'blue-grey-5',
    label: 'Working'
  },
  'Complete': {
    icon: 'done',
    color: 'green-6',
    label: 'Completed'
  },
  'Failed': {
    icon: 'thumb_down',
    color: 'red-6',
    label: 'Failed'
  },
  'Cancelled': {
    icon: 'clear',
    color: 'grey-6',
    label: 'Cancelled'
  },
  empty: {
    icon: 'done',
    color: 'pink-6',
    label: 'No Status'
  }
}
// enum Status {
//   Pending
//   Working
//   Complete
//   Failed
//   Cancelled
// }
export const PENDING = 'Pending'
export const WORKING = 'Working'
export const COMPLETE = 'Complete'
export const FAILED = 'Failed'
export const CANCELLED = 'Cancelled'
export const COMPLETED_ARRAY = [
  COMPLETE,
  FAILED,
  CANCELLED
]
export const PENDING_ARRAY = [
  PENDING,
  WORKING
]
// Field Attributes
export const DATESATTR = 'dates'
export const FILTERSATTR = 'filters'
export const CHILDRENSATTR = 'children'
export const STATUSATTR = 'status'
export const ISENTITYATTR = 'entity'

export const getFilterIcon = function (filter) {
  return filterTypeIcons[filter.__typename]
}
export const getEntityIcon = function (entity) {
  return entityTypeIcons[entity.__typename]
}
export default ({app, router, Vue}) => {
  Vue.prototype.$variables = {
    entityTypeIcons,
    completedArray: COMPLETED_ARRAY,
    pendingArray: PENDING_ARRAY,
    statusSettings,
    getEntityIcon,
    getFilterIcon

  }
}
