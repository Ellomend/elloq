import {date as datelib} from 'quasar'
export const convertDate = function (dateObject) {
  let formattedString = datelib.formatDate(dateObject, 'YYYY-MM-DD')
  return formattedString
}
export const convertFromUnix = function (unix) {
  let date = new Date(unix)
  let formattedString = datelib.formatDate(date, 'YYYY-MM-DD')
  return formattedString
}
export default ({app, router, Vue}) => {
  Vue.mixin({
    methods: {
      delayFunction (func, params) {
        setTimeout(() => {
          func(...params)
        }, 500)
      },
      humanDate (date, format = 'MMM DD ddd') {
        return datelib.formatDate(new Date(date), format)
      },
      getStatusColor (status) {
        if (!status) {
          return this.$variables.statusSettings.empty.color
        } else {
          return this.$variables.statusSettings[status].color
        }
      },
      getStatusIcon (status) {
        if (!status) {
          return this.$variables.statusSettings.empty.icon
        } else {
          return this.$variables.statusSettings[status].icon
        }
      },
      convertDate,
      convertFromUnix,
      asyncForEach: async (array, callback) => {
        for (let index = 0; index < array.length; index++) {
          await callback(array[index], index, array)
        }
      },
      waitFor: (ms) => new Promise(resolve => setTimeout(resolve, ms))
    }
  })
}
