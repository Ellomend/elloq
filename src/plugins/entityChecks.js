import {date} from 'quasar'
export const checks = {
  canHaveDates (entity) {
    return entitiesWithDates.includes(entity.__typename)
  },
  canHaveFilters (entity) {
    return entitiesWithFilters.includes(entity.__typename)
  },
  canHaveStatus (entity) {
    return entitiesWithStatus.includes(entity.__typename)
  },
  isParent (entity) {
    return parentEntities.includes(entity.__typename)
  },
  isChild (entity) {
    return childEntities.includes(entity.__typename)
  }
}
export const childEntities = [
  'Step',
  'Point'
]
export const entitiesWithDates = [
  'Task',
  'Goal',
  'Step'
]
export const entitiesWithFilters = [
  'Task',
  'Goal',
  'Checklist'
]
export const parentEntities = [
  'Goal',
  'Checklist'
]
export const entitiesWithStatus = [
  'Task',
  'Point',
  'Step'
]
export default ({app, router, Vue}) => {
  Vue.prototype.$date = date
  Vue.prototype.$entityChecks = checks
  Vue.mixin({
    methods: {
      /**
       * Is this entity indeed can ever have dates ?
       * @param entity
       */
      canHaveDates (entity) {
        return this.$entityChecks.canHaveDates(entity)
      },
      canHaveFilters (entity) {
        return this.$entityChecks.canHaveFilters(entity)
      },
      isParent (entity) {
        return this.$entityChecks.isParent(entity)
      },
      isChild: checks.isChild,
      canHaveStatus (entity) {
        return this.$entityChecks.canHaveStatus(entity)
      }
    }
  })
}
