import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

import user from './user'
import entity from './entity'
import filters from './filters'
import dates from './dates'
import logs from './logs'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    user,
    filters,
    dates,
    logs,
    entity
  },
  plugins: [createPersistedState({
    paths: ['user', 'dates', 'filters']
  })]
})

export default store
