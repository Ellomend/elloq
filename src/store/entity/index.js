import {createEntity, removeEntity, updateEntity} from '../../plugins/data/graphqlQueries'

const state = {
  formOpen: null,
  entityType: null,
  formType: null,
  editableEntity: {},
  toUpdateList: false
}
const getters = {
  formOpen: (state) => {
    return state.formOpen
  },
  createFormOpen () {
    return (state.formOpen && state.formType === 'create')
  },
  editFormOpen () {
    return (state.formOpen && state.formType === 'edit')
  },
  viewFormOpen () {
    return (state.formOpen && state.formType === 'view')
  },
  toUpdateList (state) {
    return state.toUpdateList
  }
}
const mutations = {
  openCreateForm: (state, data) => {
    state.formOpen = true
    state.formType = 'create'
    state.entityType = data.__typename
    state.editableEntity = data
  },
  // obj entity
  openEditForm (state, obj) {
    state.formOpen = true
    state.formType = 'edit'
    state.entityType = obj.__typename
    state.editableEntity = {...obj}
  },
  openViewForm (state, obj) {
    state.formOpen = true
    state.formType = 'view'
    state.entityType = obj.__typename
    state.editableEntity = {...obj}
  },
  closeForm: (state) => {
    state.formOpen = false
    state.entityType = null
    state.formType = null
    state.toUpdateList = true
  },
  listUpdated (state) {
    state.toUpdateList = false
  }
}
const actions = {
  openCreateForm ({commit, dispatch}, data) {
    commit('openCreateForm', data)
  },
  // obj entity
  openEditForm ({commit, dispatch}, obj) {
    commit('openEditForm', obj)
  },
  openViewForm ({commit, dispatch}, obj) {
    commit('openViewForm', obj)
  },
  closeForm ({commit, dispatch}) {
    commit('closeForm')
  },
  createEntity ({commit, dispatch}, entity) {
    createEntity(entity)
      .then(r => {
        if (!entity.__noClose) {
          dispatch('closeForm')
        }
      })
  },
  remove ({commit, dispatch}, entity) {
    let children = []
    if (entity.__typename === 'Goal') {
      children.push(...entity.steps)
    }
    if (entity.__typename === 'Checklist') {
      children.push(...entity.points)
    }
    removeEntity(entity)
      .then(r => {
        children.forEach(child => {
          console.log('removing child')
          removeEntity(child)
        })
        dispatch('closeForm')
      })
  },
  updateEntity ({commit, dispatch}, entity) {
    updateEntity(entity)
      .then(r => {
        if (!entity.__noClose) {
          dispatch('closeForm')
        }
      })
  },
  listUpdated ({commit}) {
    commit('listUpdated')
  }
}
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
