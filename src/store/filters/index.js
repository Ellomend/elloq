import {Blocks} from '../../plugins/data/provider/DataModels/Filters/Blocks'
import Categories from '../../plugins/data/provider/DataModels/Filters/Categories'
import Moods from '../../plugins/data/provider/DataModels/Filters/Moods'
import Projects from '../../plugins/data/provider/DataModels/Filters/Projects'
import {Contexts} from '../../plugins/data/provider/DataModels/Filters/Contexts'
import {CANCELLED, COMPLETE, FAILED, PENDING, WORKING} from '../../plugins/variables'
import {getGroupingsFilter} from '../../plugins/data/graphqlQueries/filtersFilter/filtersFilter'

const types = {
  'Block': Blocks,
  'Category': Categories,
  'Mood': Moods,
  'Project': Projects,
  'Context': Contexts
}
const update = function (entity) {
  return types[entity.__typename].update(entity)
}
const remove = function (entity) {
  return types[entity.__typename].remove(entity)
}
const create = function (entity) {
  console.log('create ', entity)
  return types[entity.__typename].add(entity)
}

const state = {
  editedFilter: null,
  apply: false,
  entityTypes: ['Task', 'Goal', 'Point', 'Checklist', 'Step'],
  entityStatuses: [PENDING, WORKING, COMPLETE, FAILED, CANCELLED],
  showToggleFiltersModal: false,
  accountedGroupings: [],
  interGrouping: 'OR',
  excludeOnly: false
}
const getters = {
  editedFilter (state) {
    return state.editedFilter
  },
  apply (state) {
    return state.apply
  },
  getShowEntityTypes (state) {
    return state.entityTypes
  },
  showToggleFiltersModal: (state) => {
    return state.showToggleFiltersModal
  },
  getGroupingsFilter (state) {
    return getGroupingsFilter(state.accountedGroupings, state.interGrouping, state.excludeOnly)
  },
  getInterGrouping (state) {
    return state.interGrouping
  }
}
const mutations = {
  openModal (state, filter) {
    state.editedFilter = filter
  },
  closeModal (state) {
    state.editedFilter = null
  },
  toggleApply (state) {
    state.apply = !state.apply
  },
  setEntityTypes (state, types) {
    state.entityTypes = types
  },
  removeEntityType (state, type) {
    state.entityTypes.slice(state.entityTypes.indexOf(type), -1)
  },
  setToggleFiltersModal (state, val) {
    state.showToggleFiltersModal = val
  },
  toggleToggleFiltersModal (state) {
    state.showToggleFiltersModal = !state.showToggleFiltersModal
  },
  setStatuses (state, val) {
    state.entityStatuses = val
  },
  setGroupings (state, groupingsArray) {
    state.accountedGroupings = groupingsArray
  },
  toggleInterGrouping (state) {
    if (state.interGrouping === 'OR') {
      state.interGrouping = 'AND'
    } else {
      state.interGrouping = 'OR'
    }
  },
  setExcludeOnly (state, val) {
    state.excludeOnly = val
  }
}
const actions = {
  edit ({commit}, filter) {
    commit('openModal', filter)
  },
  cancel ({commit}) {
    commit('closeModal')
  },
  remove ({commit}, filter) {
    remove(filter)
      .then(r => {
        commit('closeModal')
      })
  },
  openCreate ({commit}, filter) {
    commit('openModal', filter)
  },
  create ({commit}, filter) {
    create(filter)
      .then(r => {
        commit('closeModal')
      })
  },
  update ({commit}, filter) {
    update(filter)
      .then(r => {
        commit('closeModal')
      })
  },
  setEntityTypes ({commit}, type) {
    commit('setEntityTypes', type)
  },
  removeEntityType ({commit}, type) {
    commit('removeEntityType', type)
  }
}
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
