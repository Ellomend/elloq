import {Logs} from '../../plugins/data/provider/DataModels/Logs'
import {convertFromUnix} from '../../plugins/helpers'

const state = {
  openModal: false,
  modalType: null,
  editedLog: null
}
const getters = {
  isLogsModalOpen: (state, getters) => {
    return state.openModal
  },
  getModalType: (state) => {
    return state.modalType
  },
  getEditedLog: (state) => {
    return state.editedLog
  }
}
const mutations = {
  closeModal (state) {
    state.openModal = false
    state.modalType = null
    state.editedLog = null
  },
  openCreate (state) {
    state.openModal = true
    state.modalType = 'create'
  },
  openEdit (state, log) {
    state.openModal = true
    state.modalType = 'edit'
    state.editedLog = log
  }
}
const actions = {
  add ({commit}, log) {
    log.time = convertFromUnix(log.time)
    Logs.add(log)
      .then((r) => {
        commit('closeModal')
      })
      .catch(err => {
        console.log('err', err)
        commit('closeModal')
      })
  },
  remove ({commit}, log) {
    Logs.remove(log)
      .then((r) => {
        commit('closeModal')
      })
      .catch(err => {
        console.log('err', err)
        commit('closeModal')
      })
  },
  update ({commit}, log) {
    Logs.update(log)
      .then((r) => {
        console.log('updated')
        console.log(r)
        commit('closeModal')
      })
      .catch(err => {
        console.log('err', err)
        commit('closeModal')
      })
  }
}
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
