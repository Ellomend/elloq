const state = {
  user: null
}
const getters = {
  getUser: (state, getters) => {
    return state.user
  },
  getUserId (state) {
    if (state.user) {
      return state.user.id
    }
    return null
  }
}
const mutations = {
  login: (state, data) => {
    state.user = data
  },
  logout: (state) => {
    state.user = null
  }
}
const actions = {}
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
