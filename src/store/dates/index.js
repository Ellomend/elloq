import { date } from 'quasar'
import dfs from 'date-fns'
import {FILTERS_CONSTS} from '../../plugins/queryBuilder'
let getTodayDate = function () {
  let timeStamp = Date.now()
  let formattedString = date.formatDate(timeStamp, 'YYYY-MM-DD')
  return formattedString
}
const state = {
  applyDates: false,
  start: getTodayDate(),
  end: getTodayDate(),
  startType: 'start_gte',
  endType: 'end_gte',
  showDateless: false,
  now: Date.now(),
  started: false,
  rules: {},
  datesKind: FILTERS_CONSTS.TIME_KINDS.TOUCH_BOTH_SIDES
}
const getters = {
  applyDates (state) {
    return state.applyDates
  },
  getStart (state) {
    return state.start
  },
  getEnd (state) {
    return state.end
  },
  showLate (state) {
    return state.showLate
  },
  getDisplayTime (state) {
    return dfs.format(state.now, 'D ddd  :E  MMM (HH:mm:ss)')
  },
  getStartType: (state) => {
    return state.startType
  },
  getEndType: (state) => {
    return state.endType
  },
  getRules (state) {
    if (state.showDateless) {
      return {
        OR: [
          state.rules,
          {
            start: null,
            end: null
          }
        ]
      }
    }
    return state.rules
  },
  showDateless: (state) => {
    return state.showDateless
  },
  getDatesKind: (state) => {
    return state.datesKind
  }
}
const mutations = {
  toggleApplyDates (state) {
    state.applyDates = !state.applyDates
  },
  toggleShowLate (state) {
    state.showLate = !state.showLate
  },
  setStart (state, date) {
    state.start = date
  },
  setEnd (state, end) {
    state.end = end
  },
  updateTime (state) {
    if (state.started) {
      state.now = new Date()
    }
  },
  start (state) {
    state.started = true
  },
  setStartType (state, val) {
    state.startType = val
  },
  setEndType (state, val) {
    state.endType = val
  },
  setRules (state, val) {
    state.rules = {
      ...val
    }
  },
  toggleShowDateless (state) {
    state.showDateless = !state.showDateless
  },
  setDatesKind (state, kind) {
    state.datesKind = kind
  }
}
const actions = {
  start ({ commit }) {
    commit('start')
    setInterval(() => {
      commit('updateTime')
    }, 1000 * 5)
  }
}
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
