import gql from 'graphql-tag'

export const checklists = gql`
    query getUserChecklists (
    $id: ID,
    $filter: ChecklistFilter,
        $pointFilter: PointFilter
    ){
        User (id: $id) {
            checklists (
                filter: $filter
            ) {
                id
                name
                description
                points (
                    filter: $pointFilter
                ) {
                    id
                    name
                    description
                    order
                    status
                }
                type
                blocks {
                    id
                    name
                    description
                    start
                    end
                }
                categories {
                    id
                    name
                    description
                }
                projects {
                    id
                    name
                    description
                }
                moods {
                    id
                    name
                    description
                }
                contexts {
                    id
                    name
                    description
                }
            }
        }
    }
`
