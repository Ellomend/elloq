import gql from 'graphql-tag'

export const tasks = gql`
    query getUserTasks ($id: ID, $filter: TaskFilter){
        User (id: $id) {
            tasks (
                filter: $filter
            ) {
                id
                name
                description
                start
                end
                status
                description
                blocks {
                    id
                    name
                    description
                    start
                    end
                }
                categories {
                    id
                    name
                    description
                }
                projects {
                    id
                    name
                    description
                }
                moods {
                    id
                    name
                    description
                }
                contexts {
                    id
                    name
                    description
                }
            }
        }
    }
`
