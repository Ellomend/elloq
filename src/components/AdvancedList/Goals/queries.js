import gql from 'graphql-tag'

export const goals = gql`
    query getUserGoals (
    $id: ID,
    $filter: GoalFilter,
    $stepFilter: StepFilter
    ){
        User (id: $id) {
            goals (
                filter: $filter
            ) {
                id
                name
                description
                start
                end
                steps (
                    filter: $stepFilter
                ) {
                    id
                    name
                    description
                    start
                    end
                    status
                }
                blocks {
                    id
                    name
                    description
                    start
                    end
                }
                categories {
                    id
                    name
                    description
                }
                projects {
                    id
                    name
                    description
                }
                moods {
                    id
                    name
                    description
                }
                contexts {
                    id
                    name
                    description
                }
            }
        }
    }
`
