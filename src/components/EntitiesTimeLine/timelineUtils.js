import {getEntityIcon} from '../../plugins/variables'

export const makeItem = (entity) => {
  return {
    ...entity
  }
}
export const makeTaskItem = (entity) => {
  return {
    id: entity.id,
    group: 0,
    name: entity.name,
    start: Date.parse(entity.start),
    end: Date.parse(entity.end),
    className: `st${entity.status}  _t${entity.__typename} _ltEntity`,
    content: `<i aria-hidden="true" class="q-icon material-icons" style="font-size: 14px;">${getEntityIcon({__typename: 'Task'})}</i>
${entity.name}`,
    __entity: entity
  }
}

export const makeGroup = (goal, index) => {
  return {
    id: `GL${goal.id}`,
    content: `G ${goal.name}`,
    start: goal.start,
    end: goal.end,
    type: 'background',
    className: 'negative'
  }
}
export const makeStep = (entity) => {
  return {
    id: entity.id,
    group: 0,
    name: entity.name,
    start: Date.parse(entity.start),
    end: Date.parse(entity.end),
    content: `<i aria-hidden="true" class="q-icon material-icons" style="font-size: 14px;">${getEntityIcon({__typename: 'Step'})}</i> ${entity.name}`,
    className: `st${entity.status}`,
    __entity: entity
  }
}

export const filterByType = (entity, type) => {
  return entity.__typename === type
}

export const goalFilter = (entity) => {
  return filterByType(entity, 'Goal')
}

export const makeLog = (log) => {
  return {
    id: log.id,
    group: 0,
    name: log.type,
    start: Date.parse(log.time),
    content: `<i aria-hidden="true" class="q-icon material-icons" style="font-size: 14px;">${getEntityIcon({__typename: 'Log'})}</i> ${log.comment.slice(0, 10)}`,
    className: `st${log.kind}`,
    __entity: log
  }
}
