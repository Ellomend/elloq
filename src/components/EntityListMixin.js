import EntityCard from '../components/EntityCard'
import EntitiesTable from '../components/EntitiesTable'
import {stuff} from '../plugins/graphql'
import EntitiesCalendar from '../components/EntitiesCalendar'
import GoalCard from '../components/GoalCard'
import GroupByDate from '../components/GroupByDate'
import TimeLine from '../components/TimeLine'
import LogCard from '../components/LogCard'
export default {
  components: {LogCard, TimeLine, GroupByDate, EntitiesCalendar, EntitiesTable, EntityCard, GoalCard},
  data () {
    return {
      list: [],
      listType: 'table',
      loading: false,
      checklistTypes: [1],
      checklistTypesOptions: [0, 1],
      showSteps: false,
      queryState: stuff.ALL_ENTITIES_QUERY
    }
  },
  apollo: {
    list: {
      query () {
        return this.queryState
      },
      variables () {
        return this.totalVariable
      },
      fetchPolicy: 'network-only',
      update (r) {
        let user = r.User
        let entities = []
        if (user.tasks && user.tasks.length) {
          user.tasks.forEach(task => {
            entities.push({...task})
          })
        }
        user.checklists.forEach(cl => {
          entities.push({...cl})
          cl.points.forEach(point => {
            entities.push({...point})
          })
        })
        if (user.goals && user.goals.length) {
          user.goals.forEach(goal => {
            entities.push({...goal})
            goal.steps.forEach(step => {
              entities.push({...step})
            })
          })
        }
        if (user.logs && user.logs.length) {
          user.logs.forEach(log => {
            entities.push({...log})
          })
        }
        return entities
      },
      watchLoading (isLoading, countModifier) {
        if (isLoading) {
          this.$q.loading.show()
        } else {
          this.$q.loading.hide()
        }
      },
      result ({data, loading, networkStatus}) {
        this.$q.notify({
          // only required parameter is the message:
          message: `Refetched`,
          timeout: 1000, // in milliseconds; 0 means no timeout
          type: 'positive',
          color: 'positive',
          textColor: 'white', // if default 'white' doesn't fits
          icon: 'wifi',
          position: 'top-right' // 'top', 'left', 'bottom-left' etc
        })
        this.refetchedList()
      },
      // Error handling
      error (error) {
        this.$q.notify({
          // only required parameter is the message:
          message: `entities not loaded: ${error.message}`,
          timeout: 1000, // in milliseconds; 0 means no timeout
          type: 'positive',
          color: 'positive',
          textColor: 'black', // if default 'white' doesn't fits
          icon: 'wifi',
          position: 'top-right' // 'top', 'left', 'bottom-left' etc
        })
      }
    }
  },
  computed: {
    totalVariable () {
      return {
        userId: this.userId,
        checklistFilter: this.checklistFilter,
        goalFilter: this.goalFilter,
        taskFilter: this.taskFilter,
        pointFilter: this.pointFilter,
        stepFilter: this.stepFilter,
        logFilter: this.logFilter
      }
    },
    // filter by type
    filteredList () {
      let result = this.list.filter(entity => {
        return this.entityTypes.includes(entity.__typename)
      })
      return result
    },
    datesFilter () {
      if (!this.$store.getters['dates/applyDates']) {
        return {}
      }
      return this.$store.getters['dates/getRules']
    },
    statusFilter () {
      return {
        status_in: this.$store.state.filters.entityStatuses
      }
    },
    // Done
    taskFilter () {
      return {
        AND: [
          this.datesFilter,
          this.statusFilter,
          this.filtersFilter
        ]
      }
    },
    // Done
    goalFilter () {
      return {
        AND: [
          this.filtersFilter,
          this.entityTypes.includes('Step') ? {} : this.datesFilter
        ]
      }
    },
    checklistFilter () {
      return {
        AND: [
          this.filtersFilter,
          {
            'type_in': this.checklistTypes
          }
        ]
      }
    },
    pointFilter () {
      return {
        AND: [
          this.statusFilter
        ]
      }
    },
    stepFilter () {
      return {
        AND: [
          this.statusFilter,
          !this.entityTypes.includes('Step') ? {} : this.datesFilter
        ]
      }
    },
    logFilter () {
      let filter = {}
      filter['time_gte'] = this.$store.getters['dates/getStart']
      filter['time_lte'] = this.$store.getters['dates/getEnd']
      return this.applyDates ? filter : {}
    },
    entityTypes: {
      get () {
        return this.$store.getters['filters/getShowEntityTypes']
      },
      set (val) {
        // this.$store.commit('moduleName/mutationName', attribute)
      }
    },
    start: {
      get () {
        return this.$store.getters['dates/getStart']
      },
      set (val) {
        this.$store.commit('dates/setStart', val)
      }
    },
    end: {
      get () {
        return this.$store.getters['dates/getEnd']
      },
      set (val) {
        this.$store.commit('dates/setEnd', val)
      }
    },
    applyDates: {
      get () {
        return this.$store.getters['dates/applyDates']
      },
      set () {
        this.$store.commit('dates/toggleApplyDates')
      }
    },
    apply: {
      get () {
        return this.$store.getters['filters/apply']
      },
      set () {
        this.$apollo.queries.list.refetch()
        this.$store.commit('filters/toggleApply')
      }
    },
    filtersFilter () {
      return this.$store.getters['filters/getGroupingsFilter']
    }
  },
  methods: {
    delayedRefetch () {
      setTimeout(() => {
        this.$apollo.queries.list.refetch()
      }, 300)
    },
    refetchedList () {
      console.log('refetched handler')
    }
  },
  mounted () {
    this.$root.$on('refetch_entities', this.delayedRefetch)
    this.$root.$on('refetch_list', this.delayedRefetch)
  },
  destroyed () {
    this.$root.$off('refetch_entities', this.delayedRefetch)
    this.$root.$off('refetch_list', this.delayedRefetch)
  }
}
