
export default [
  {
    path: '/',
    component: () => import('layouts/default'),
    children: [
      {
        path: '',
        name: 'home',
        meta: {
          public: true
        },
        component: () => import('pages/index')
      },
      {
        path: 'list',
        name: 'EntityListPage',
        meta: {
          public: false
        },
        component: () => import('pages/EntityListPage')
      },
      {
        path: 'memes',
        name: 'MemesPage',
        meta: {
          public: false
        },
        component: () => import('pages/MemesPage')
      },
      {
        path: 'trash',
        name: 'TrashPage',
        meta: {
          public: false
        },
        component: () => import('pages/TrashPage')
      },
      {
        path: 'filters',
        name: 'FiltersPage',
        meta: {
          public: false
        },
        component: () => import('pages/FiltersPage')
      },
      {
        path: '/login',
        name: 'login',
        meta: {
          public: true
        },
        component: () => import('pages/LoginPage')
      },
      {
        path: '/register',
        name: 'register',
        meta: {
          public: true
        },
        component: () => import('pages/RegisterPage')
      },
      {
        path: '/checklists',
        name: 'checklists',
        meta: {
          public: false
        },
        component: () => import('pages/checklists')
      },
      {
        path: '/blocks',
        name: 'blocks',
        meta: {
          public: false
        },
        component: () => import('pages/BlocksPage')
      },
      {
        path: '/categories',
        name: 'categories',
        meta: {
          public: false
        },
        component: () => import('pages/CategoriesPage')
      },
      {
        path: '/checklist/:id',
        name: 'viewChecklist',
        meta: {
          public: false
        },
        component: () => import('pages/viewChecklist')
      },
      {
        path: '/goals',
        name: 'goals',
        meta: {
          public: false
        },
        component: () => import('pages/goalsPage')
      },
      {
        path: '/3years',
        name: '3years',
        meta: {
          public: false
        },
        component: () => import('pages/3years')
      },
      {
        path: '/logs',
        name: 'logs',
        meta: {
          public: false
        },
        component: () => import('pages/logsPage')
      },
      {
        path: 'filter/:type/:id/',
        name: 'FilterDetailsPage',
        meta: {
          public: false
        },
        component: () => import('pages/FilterDetailsPage')
      },
      {
        path: 'today',
        name: 'today',
        meta: {
          public: false
        },
        component: () => import('pages/TodayPage')
      },
      {
        path: 'late',
        name: 'late',
        meta: {
          public: false
        },
        component: () => import('pages/LatePage')
      },
      {
        path: 'now',
        name: 'now',
        meta: {
          public: false
        },
        component: () => import('pages/NowPage')
      },
      {
        path: 'advancedList',
        name: 'advancedList',
        meta: {
          public: false
        },
        component: () => import('pages/advancedList')
      },
      {
        path: 'chartsPage',
        name: 'chartsPage',
        meta: {
          public: false
        },
        component: () => import('pages/ChartsPage')
      }
    ]
  },

  { // Always leave this as last one
    path: '*',
    component: () => import('pages/404')
  }
]
