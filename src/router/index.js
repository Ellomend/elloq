import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store/index'
import routes from './routes'

Vue.use(VueRouter)

const Router = new VueRouter({
  /*
   * NOTE! Change Vue Router mode from quasar.conf.js -> build -> vueRouterMode
   *
   * When going with "history" mode, please also make sure "build.publicPath"
   * is set to something other than an empty string.
   * Example: '/' instead of ''
   */

  // Leave as is and change from quasar.conf.js instead!
  mode: process.env.VUE_ROUTER_MODE,
  base: process.env.VUE_ROUTER_BASE,
  scrollBehavior: () => ({ y: 0 }),
  routes
})
Router.beforeEach((to, from, next) => {
  // user not logged in - redirect to login
  if (to.meta && to.meta.public !== true && !store.getters['user/getUser']) {
    // TODO fix this
    next({ name: 'home' })
    next({ name: 'login' })
  }
  next()
})
export default Router
